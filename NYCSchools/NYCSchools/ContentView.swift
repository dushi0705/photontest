//
//  ContentView.swift
//  NYCSchools
//
//  Created by Dushyanth Challagundla on 1/25/24.
//

import SwiftUI
import Combine

struct ContentView: View {
    
    @ObservedObject var viewModel = SchoolsViewModel()
    
    var body: some View {
        NavigationView {
            List(viewModel.schools, id:\.dbn) { school in
                NavigationLink(destination: SchoolDetailView(school: school)) {
                    VStack(alignment: .leading) {
                        Text(school.school_name ?? "").font(.headline)
                        Text(school.dbn ?? "").font(.subheadline).foregroundColor(.gray)
                    }
                }
            }
            .navigationTitle("Schools")
            .onAppear() {
                viewModel.fetchData()
            }
        }
    }
}

#Preview {
    ContentView()
}
