//
//  NetworkMangerMock.swift
//  NYCSchools
//
//  Created by Dushyanth Challagundla on 2/2/24.
//

import Foundation

class NetworkMangerMock : ServiceProtocol {
    var shouldSucced = true


    func fetchData(completion: @escaping (Result<[SchoolModel], Error>) -> Void) {
        if shouldSucced {
            let schoolModel:[SchoolModel] = [SchoolModel(id: "1234",dbn: "DBN1234", school_name: "NYC School", overview_paragraph: "About Scholl "), SchoolModel(id: "5678",dbn: "DBN5679", school_name: "NYC public School", overview_paragraph: "description")]
            completion(.success(schoolModel))
        } else {
            let err = NSError(domain: "com.personal.nycschools", code: 0)
            completion(.failure(err))
        }
        
        
    }
}
