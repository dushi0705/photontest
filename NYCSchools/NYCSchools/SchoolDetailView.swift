//
//  SchoolDetailView.swift
//  NYCSchools
//
//  Created by Dushyanth Challagundla on 1/25/24.
//

import Foundation
import SwiftUI
import Combine

struct SchoolDetailView: View {
    var school:SchoolModel
    
    var body: some View {
        NavigationView {
            VStack(alignment: .leading, spacing: 16) {
                Text(school.school_name ?? "").font(.headline)
                Text(school.dbn ?? "").font(.subheadline).foregroundColor(.gray)
                Spacer()
                Text(school.overview_paragraph ?? "").font(.subheadline).foregroundColor(.gray)
            }
            .padding()
            //.navigationBarTitle(Text("School Details"), displayMode:.inline)
            
        }
    }
}

