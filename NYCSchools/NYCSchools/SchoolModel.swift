//
//  SchoolModel.swift
//  NYCSchools
//
//  Created by Dushyanth Challagundla on 1/25/24.
//

import Foundation

struct SchoolModel : Decodable, Identifiable {
    var id: String? = UUID().uuidString
    var dbn: String?
    var school_name:String?
    var overview_paragraph:String?

    /*enum codingKeys: String, CodingKey {
        case id
        case dbn
        case schoolName = "school_name"
    }*/
    init(id: String? = nil, dbn: String? = nil, school_name: String? = nil, overview_paragraph: String? = nil) {
        self.id = id
        self.dbn = dbn
        self.school_name = school_name
        self.overview_paragraph = overview_paragraph
    }
}
