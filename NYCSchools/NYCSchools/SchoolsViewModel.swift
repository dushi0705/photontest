//
//  SchoolsViewModel.swift
//  NYCSchools
//
//  Created by Dushyanth Challagundla on 1/25/24.
//

import Foundation


class SchoolsViewModel: ObservableObject {
    @Published var schools: [SchoolModel] = []
    @Published var errorMessage: String = ""
    var networkManger: ServiceProtocol
    init(service: ServiceProtocol =  NetworkManager.shared) {
        self.networkManger = service
    }
    
    func fetchData() {
        networkManger.fetchData { [weak self]result in
            switch result {
            case .success(let schools):
                DispatchQueue.main.async {
                    self?.schools = schools
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self?.errorMessage = "failed to get the schools"
                }
                
                print("Handle error case here")
            }
        }
    }
}
