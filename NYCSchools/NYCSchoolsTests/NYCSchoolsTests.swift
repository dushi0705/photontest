//
//  NYCSchoolsTests.swift
//  NYCSchoolsTests
//
//  Created by Dushyanth Challagundla on 2/2/24.
//

import XCTest
@testable import NYCSchools

final class NYCSchoolsTests: XCTestCase {
    var viewModel: SchoolsViewModel!
    var mockService: NetworkMangerMock!

    override func setUp() {
        super.setUp()
        mockService = NetworkMangerMock()
        viewModel = SchoolsViewModel(service: mockService)
        
    }
    override func tearDown() {
        viewModel = nil
        mockService = nil
    }
    func testSchoolsService() {
        XCTAssertNotNil(viewModel)
        XCTAssertNotNil(mockService)
        viewModel.fetchData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            XCTAssertNotNil(self.viewModel.schools)
            let firstObject = self.viewModel.schools.first
            XCTAssertEqual(firstObject?.school_name,  "NYC School")
            XCTAssertEqual(firstObject?.dbn,  "DBN1234")
            XCTAssertGreaterThanOrEqual(self.viewModel.schools.count, 2)
        }
        
    }
    func testFetchUserFailure() {
        mockService.shouldSucced = false
        XCTAssertFalse(mockService.shouldSucced)
        viewModel.fetchData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            XCTAssertNotNil(self.viewModel.errorMessage)
            XCTAssertEqual(self.viewModel.errorMessage, "failed to get the schools")
        }
        
        
        
    }
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}
