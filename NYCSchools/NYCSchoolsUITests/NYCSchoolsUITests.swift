//
//  NYCSchoolsUITests.swift
//  NYCSchoolsUITests
//
//  Created by Dushyanth Challagundla on 1/25/24.
//

import XCTest
@testable import NYCSchools

final class NYCSchoolsUITests: XCTestCase {
//    var viewModel: SchoolsViewModel!
//    
//    override func setUpWithError() throws {
//            viewModel = SchoolsViewModel()
//        }
//
//        override func tearDownWithError() throws {
//            viewModel = nil
//        }
/*
    func testFetchSchools() {
        // Initialize the view model
        viewModel = SchoolsViewModel()

       
        let mockData = """
        [
            {"id": "1", "schoolName": "ABC", "dbn": "12322", "overviewParagraph": "Overview1"},
            {"id": "2", "schoolName": "School 2", "dbn": "45621", "overviewParagraph": "Overview2"}
        ]
        """.data(using: .utf8)

        _ = MockURLSession(data: mockData, response: nil, error: nil)

        viewModel.fetchData()

        

        // Assertions
        XCTAssertEqual(viewModel.schools.count, 2)
        XCTAssertEqual(viewModel.schools[0].school_name, "School 1")
        XCTAssertEqual(viewModel.schools[1].dbn, "456")
    }
 */
}
class MockURLSession: URLSession {
    var data: Data?
    var response: URLResponse?
    var error: Error?

    init(data: Data?, response: URLResponse?, error: Error?) {
        self.data = data
        self.response = response
        self.error = error
        
    }

    override func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        let task = MockURLSessionDataTask {
            completionHandler(self.data, self.response, self.error)
        }
        return task
    }
}
class MockURLSessionDataTask: URLSessionDataTask {
    private let closure: () -> Void

    init(closure: @escaping () -> Void) {
        self.closure = closure
    }

    override func resume() {
        closure()
    }
}
